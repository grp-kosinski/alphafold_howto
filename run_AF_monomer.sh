#!/bin/bash

#A typical runs takes up to a couple of hours but may be much longer
#SBATCH --time=1-00:00:00

#log files:
#SBATCH -e %x_%j_err.txt
#SBATCH -o %x_%j_out.txt

#qos sets priority, you can set to high or highest but there is a limit of high priority jobs per user: https://wiki.embl.de/cluster/Slurm#QoS
#SBATCH --qos=normal

#For long sequences, use other settings according to: https://wiki.embl.de/cluster/Hardware. 
#SBATCH -p gpu-el8
#SBATCH -C gaming

#Reserve the entire GPU so no-one else slows you down
#SBATCH --gres=gpu:1

#Limit the run to a single node
#SBATCH -N 1

#Adjust this depending on the node
#SBATCH --ntasks=8
#SBATCH --mem=64276


module load AlphaFold

module list

GANGLIASTARTTIME=$(date "+%m%%2F%d%%2F%Y+%H%%3A:M")
GANGLIAMACHINE=$(hostname -s | awk '{print toupper($1)}')
GANGLIAGPUS=$(echo "$CUDA_VISIBLE_DEVICES" | sed -e "s/,/%2C/g")
STARTURLGPU="http://web.cluster.embl.de/ganglia/graph.php?r=hour&z=xlarge&title=GPU_util&vl=&x=&n=&hreg[]=$(hostname)&mreg[]=gpu[${GANGLIAGPUS}]_util&gtype=line&glegend=show&aggregate=1&embed=1&cs=$GANGLIASTARTTIME"

echo "Your Job $SLURM_JOB_NAME $SLURM_JOBID is running on node $(hostname -s) using the GPU device(s) $CUDA_VISIBLE_DEVICES"; echo "Please monitor the GPU utilization on: $STARTURLGPU"

export ALPHAFOLD_JACKHMMER_N_CPU=$SLURM_NTASKS
export ALPHAFOLD_HHBLITS_N_CPU=$SLURM_NTASKS

# If you use --cpus-per-task=X and --ntasks=1 your script should contain:
# export ALPHAFOLD_JACKHMMER_N_CPU=$SLURM_CPUS_PER_TASK
# export ALPHAFOLD_HHBLITS_N_CPU=$SLURM_CPUS_PER_TASK

# TF_FORCE_UNIFIED_MEMORY='1' XLA_PYTHON_CLIENT_MEM_FRACTION are optional but may be necessary for bigger sequences.
MAXRAM=$(bc <<< "$(ulimit -m) / 1024.0")
GPUMEM=$(nvidia-smi --query-gpu=memory.total --format=csv,noheader,nounits | tail -1)
# Check if nvidia-smi command succeeded
if [ $? -ne 0 ]; then
    echo "Error: Failed to query GPU memory."
    exit 1
fi
export XLA_PYTHON_CLIENT_MEM_FRACTION=$(echo "scale=3; $MAXRAM / $GPUMEM" | bc)
# Check if calculation succeeded
if [ $? -ne 0 ]; then
    echo "Error: Failed to calculate memory fraction."
    exit 1
fi
export TF_FORCE_UNIFIED_MEMORY='1'


echo 'MAXRAM:' $MAXRAM
echo 'GPUMEM:' $GPUMEM
echo 'XLA_PYTHON_CLIENT_MEM_FRACTION:' $XLA_PYTHON_CLIENT_MEM_FRACTION


#If you read this after 2050-01-01, probably you want to adjust the date
time run_alphafold.py --fasta_paths=your_fasta.fasta --output_dir=/scratch/<your folder name> --model_preset=monomer_ptm --max_template_date=2050-01-01 
