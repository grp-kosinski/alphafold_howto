#!/bin/bash

#A typical runs takes couple of hours but may be much longer
#SBATCH --time=1-00:00:00

#log files:
#SBATCH -e %x_%j_err.txt
#SBATCH -o %x_%j_out.txt

#qos sets priority, you can set to high or highest but there is a limit of high priority jobs per user: https://wiki.embl.de/cluster/Slurm#QoS
#SBATCH --qos=normal

#SBATCH -p htc-el8

#Limit the run to a single node
#SBATCH -N 1

#Adjust this depending on the node
#SBATCH --ntasks=8
#SBATCH --mem=32000

module load AlphaFold

module list

export ALPHAFOLD_JACKHMMER_N_CPU=$SLURM_NTASKS
export ALPHAFOLD_HHBLITS_N_CPU=$SLURM_NTASKS

# If you use --cpus-per-task=X and --ntasks=1 your script should contain:
# export ALPHAFOLD_JACKHMMER_N_CPU=$SLURM_CPUS_PER_TASK
# export ALPHAFOLD_HHBLITS_N_CPU=$SLURM_CPUS_PER_TASK

START_TIME=$(date +%s)
echo "Start time: $(date)"

#If you read this after 2050-01-01, probably you want to adjust the date
time run_alphafold.py --fasta_paths=your_fasta.fasta --output_dir=/scratch/<your folder name> --model_preset=multimer --max_template_date=2050-01-01

END_TIME=$(date +%s)
echo "End time: $(date)"
DURATION=$((END_TIME - START_TIME))
DURATION_MINUTES=$(echo "scale=2; $DURATION / 60" | bc)
echo "Duration: $DURATION_MINUTES minutes"
