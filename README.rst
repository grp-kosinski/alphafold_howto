Run using AlphaPulldown
=======================

.. warning::

    Kosinski group recommends running AlphaFold using their AlphaPulldown pipeline:
    https://github.com/KosinskiLab/AlphaPulldown
    It is just way more convenient and user-friendly than the original AlphaFold.

    Quick start here using AlphaPulldown module will follow.

Run using a graphical interface
===============================

An EMBL-internal graphical interface for running AlphaFold by Grzegorz Chojnowski @EMBL Hamburg:
https://www.embl.org/internal-information/it-services/alphafold2-at-embl-hamburg/
Accessible for everyone at EMBL.

Run AlphaFold monomer (original DeepMind's version)
===================================================

.. warning::

    These are recently updated scripts - please be cautious and report any issues at https://git.embl.de/grp-kosinski/alphafold_howto/-/issues

This runs the original AlphaFold version, but splitting it into two steps: generating input features on CPU and running the modeling on the GPU.

1. Copy Slurm sbatch scripts ``gen_AF_features_monomer.sh`` and ``run_AF_monomer.sh``
2. Adjust the name of the FASTA file with a single sequence and output folder name
3. Adjust job submission options if necessary
4. Generate input features (multiple sequence alignments, templates) using CPU nodes:
    
    .. code-block:: bash

        sbatch gen_AF_features_monomer.sh

    This will generate a directory with the same name as the FASTA file, containing the input features for AlphaFold.
    
5. Perform modeling using GPU nodes:
    
    .. code-block:: bash

        sbatch run_AF_monomer.sh

    .. tip:: 
        
        ``--fasta_paths=`` option in the scripts can point to a comma separated list of FASTA files of multiple proteins.
        I haven't used it though so do not know whether they will be run in parallel or consecutively. 
        If the consecutively, probably you want to submit the jobs as separate commands to run in parallel.

Run AlphaFold multimer (original DeepMind's version)
====================================================

.. warning::

    These are recently updated scripts - please be cautious and report any issues at https://git.embl.de/grp-kosinski/alphafold_howto/-/issues

This runs the original AlphaFold version, but splitting it into two steps: generating input features on CPU and running the modeling on the GPU.

1. Copy Slurm sbatch scripts ``gen_AF_features_multimer.sh`` and ``run_AF_multimer.sh``

2. Prepare FASTA file with with sequences of the multimer

    The FASTA file needs to contain all sequences for a complex like the following for a heterodimer::

        >seq1
        XXXXXXX
        >seq2
        XXXXXXX

    For homo-oligomers, just repeat the same sequence multiple times, e.g. for a trimer::

        >seq1
        XXXXXXX
        >seq1
        XXXXXXX
        >seq1
        XXXXXXX

3. Adjust the name of the FASTA file and output folder name
4. Adjust job submission options if necessary
5. Generate input features (multiple sequence alignments, templates) using CPU nodes:
    
    .. code-block:: bash

        sbatch gen_AF_features_multimer.sh

    This will generate a directory with the same name as the FASTA file, containing the input features for AlphaFold.

6. Perform modeling using GPU nodes:
    
    .. code-block:: bash

        sbatch run_AF_multimer.sh

    .. tip:: 
        
        ``--fasta_paths=`` option in the scripts can point to a comma separated list of FASTA files of multiple proteins.
        I haven't used it though so do not know whether they will be run in parallel or consecutively. 
        If the consecutively, probably you want to submit the jobs as separate commands to run in parallel.



Run local ColabFold monomer or multimer
=======================================

1. Install:

    .. code-block:: bash
        
        module load Anaconda3
        conda create --name ColabFold
        source activate ColabFold

        #Below instructions copied from https://github.com/YoshitakaMo/localcolabfold on 17. May
        conda install -c conda-forge python=3.10 cudnn==8.8.0.121 cudatoolkit==11.8.0 openmm==7.7.0 pdbfixer -y
        # install alignment tools
        conda install -c conda-forge -c bioconda kalign2=2.04 hhsuite=3.3.0 mmseqs2=14.7e284 -y
        # install ColabFold and Jaxlib
        # python-m pip install "colabfold[alphafold] @ git+https://github.com/sokrypton/ColabFold"
        python -m pip install --upgrade pip
        python -m pip install --no-warn-conflicts "colabfold[alphafold-minus-jax] @ git+https://github.com/sokrypton/ColabFold" tensorflow==2.12.0
        python -m pip install https://storage.googleapis.com/jax-releases/cuda11/jaxlib-0.3.25+cuda11.cudnn82-cp310-cp310-manylinux2014_x86_64.whl
        python -m pip install jax==0.3.25 chex==0.1.6 biopython==1.79

    Set how Matplotlib will render images:

    Open (or create your matplotlibrc) file that might be:

    .. code-block:: bash

        ~/.config/matplotlib/matplotlibrc

    and add a line like this:

    .. code-block:: bash

        backend      : Agg

2. Create a new directory for your job
3. Prepare FASTA file with a single sequence. For multimer, concatenate sequences of the different proteins separated by a semicolon ":", just as with usual ColabFold
4. Copy the ``sbatch`` template ``run_ColabFold.sh``
5. Adjust the name of the FASTA file 
6. Adjust job submission options if necessary
7. Adjust ``colabfold_batch`` options

    .. tip::

        Use ``colabfold_batch --help`` to see all options.

    .. tip::

        To run the old ColabFold for complexes, equivalent to the original https://colab.research.google.com/github/sokrypton/ColabFold/blob/main/beta/AlphaFold2_advanced.ipynb,
        add ``--model-type AlphaFold2-ptm`` option to the ``colabfold_batch`` command.
        This version is less accurate and less sensitive, but does not suffer from the steric clash bug.

8. Run ``sbatch run_ColabFold.sh``


    .. warning::

        Do not run too many ColabFold jobs in parallel. It uses an external public MMSQE2 server for generating alignments,
        so it may overload that server, perhaps even blocking our IP address.

Using custom templates with ColabFold
-------------------------------------
    .. warning::

        ColabFold can use templates for either monomer or multimer modes, but does not use the relative orientation of chains in the multimeric templates.


1. Create a directory for your templates

2. In this directory place your templates so they have:

    - PDB or mmCIF format
    - have only four characters in the filename, like: 1xxx.cif
    - because of some ColabFold bug, make sure chain ids are named starting from A: A, B, C and so on in PDB and both label_asym_id and auth_asym_id are    the same in the case of mmCIF format

3. Add the ``--custom-template-path <your templates directory>`` option to the ``colabfold_batch`` command, and run the same way.

    - Note, the ``--templates`` option still must be kept. ColabFold will still look for templates on the remote server, but that's ok - it will use custom templates in the end (yeah, that's wasteful)

Run old local ColabFold multimer
================================

This is rather not useful anymore, kept for backward compatibility. 

Install and run following instructions from: https://github.com/jkosinski/alphafold/blob/sokrypton_alphafold2_advanced/README.md
 
Run FoldDock
============

Run OpenFold
============

Run RoseTTAfold 
===============
## To run RoseTTAFold2 at HD cluster:

1. Go to RF2 working directory:
`cd /g/kosinski/dima/RF2/RoseTTAFold2`

2. Create conda environment using `RF_min.yml` file:
`conda env create -f RF2-linux.min.yml`
The full package is summarized at RF2-linux.full.yml for your reference.

3. Activate conda environment:
`conda activate RF2`

4. You also need to install NVIDIA's SE(3)-Transformer (**please use SE3Transformer in this repo to install**).
 `cd SE3Transformer`
 `pip install --no-cache-dir -r requirements.txt`
 `python setup.py install`
 **You may want to compile dgl library from source code to get half precision support (recommended to handle protein > 1000 aa)**
  
5. Download and install third-party software:
 `./install_dependencies.sh`

6. Download the script to your Projectdir from here (https://git.embl.de/grp-kosinski/alphafold_howto)

7. Edit PATH_TO_RF2NA and point to your installation path <- TODO: make PATH_TO_RF2NA argument to the sbatch script

8. Run it:
`cd Projectdir`
`sbatch -J job_name run_RF2_monomer.sh input.fasta /PATH/TO/OUTPUT/DIRECTORY/`


Run RoseTTAFold2NA
===============

1. Go to RF2 working directory:
`cd /g/kosinski/dima/RF2/RoseTTAFold2NA`

2. Create conda environment using `RF2na-linux.yml` file:
`conda env create -f RF2na-linux.yml`

3. Activate conda environment:
`conda activate RF2NA`

4. You also need to install NVIDIA's SE(3)-Transformer (**please use SE3Transformer in this repo to install**).
 `cd SE3Transformer`
 `pip install --no-cache-dir -r requirements.txt`
 `python setup.py install`

5. Download the script to your Projectdir from here (https://git.embl.de/grp-kosinski/alphafold_howto) and run it:
`cd Projectdir`
`sbatch -J job_name run_RF2NA_slurm.sh /PATH/TO/OUTPUT/DIRECTORY/ P:protein.fa R:DNA.fa D:DNA.fa`
here P stands for proteins, D for DNA and R for RNA
the script takes any number of fasta files. 

Locations of databases for RoseTTAFold2(NA) is at:
`/scratch/RoseTTAFold_DBs`

The weights are pre-downloaded to 
`/g/kosinski/dima/RoseTTAFold2*/network/weights`


ColabFold setup for development
===============================

It is weird, yes. See more at:
https://github.com/sokrypton/ColabFold/blob/main/Contributing.md

1. Install

.. code-block:: bash

    module load AlphaFold
    module load GCC/10.2.0
    module load tqdm
    module load matplotlib

    DEVEL_DIR=<your dir>
    cd $DEVEL_DIR
    git clone https://github.com/steineggerlab/alphafold.git AlphaFold_ColabFold
    git clone https://github.com/jkosinski/ColabFold.git
    mkdir site-packages
    pip install "colabfold[alphafold] @ git+https://github.com/sokrypton/ColabFold" \
        --no-deps \
        --target=site-packages
    pip install alphafold-colabfold --no-deps --target=site-packages
    rm -r site-packages/colabfold
    ln -s $DEVEL_DIR/ColabFold/colabfold site-packages/colabfold
    rm -r site-packages/alphafold
    ln -s $DEVEL_DIR/AlphaFold_ColabFold/alphafold site-packages/alphafold

2. Use:

.. code-block:: bash

    module load AlphaFold
    module load GCC/10.2.0
    module load tqdm
    module load matplotlib
    export PYTHONPATH=$DEVEL_DIR/site-packages:$PYTHONPATH
    export PYTHONPATH=$DEVEL_DIR/site-packages/alphafold:$PYTHONPATH

