#!/bin/bash
#SBATCH -p gpu-el8
#SBATCH -N 1      # do not distribute on multiple nodes
#SBATCH -n 8
#SBATCH --mem=32GB
#SBATCH -C gaming
#SBATCH -t 0-12:0:0
#SBATCH --gres=gpu:1
#SBATCH -e %x-%j.err
#SBATCH -o %x-%j.out
#only limited number of jobs are allowed with qos=highest per user! 
#SBATCH --qos=normal

#example1 (run for max 12 hours on either 3090 gpu or 2080Ti GPU with 8 cores for monomer)
#  cd Projectdir
#  sbatch -J job_name run_RF2NA_slurm.sh /PATH/TO/OUTPUT/DIRECTORY/ P:protein.fa R:DNA.fa D:DNA.fa
#  here P stands for proteins, D for DNA and R for RNA

GANGLIASTARTTIME=`date "+%m%%2F%d%%2F%Y+%H%%3A%M"`
GANGLIAMACHINE=`hostname -s|awk '{print toupper($1)}'`
GANGLIAGPUS=`echo $CUDA_VISIBLE_DEVICES|sed -e "s/,/%2C/g"`
STARTURLGPU='http://web.cluster.embl.de/ganglia/graph.php?r=hour&z=xlarge&title=GPU_util&vl=&x=&n=&hreg[]='`hostname`'&mreg[]=gpu['$GANGLIAGPUS']_util&gtype=line&glegend=show&aggregate=1&embed=1&cs='$GANGLIASTARTTIME
PATH_TO_RF2NA='/g/kosinski/dima/RF2/RoseTTAFold2NA'

echo "Your Job $SLURM_JOB_NAME $SLURM_JOBID is running on node `hostname -s` using the GPU device(s) $CUDA_VISIBLE_DEVICES"; echo "Please monitor the GPU utilization on: $STARTURLGPU" 

#TODO: GPU is not always used for 100%
#TODO: add parameters for setting pointer to dbs

module load Anaconda3
eval "$(conda shell.bash hook)"
#conda env RF2NA must be pre-created and other steps from README.md must be done

# TF_FORCE_UNIFIED_MEMORY='1' XLA_PYTHON_CLIENT_MEM_FRACTION are optional but may be necessary for bigger sequences.
export TF_FORCE_UNIFIED_MEMORY='1'
MAXRAM=$(echo `ulimit -m` '/ 1048576.0'|bc)
GPUMEM=`nvidia-smi --query-gpu=memory.total --format=csv,noheader,nounits|tail -1`
export XLA_PYTHON_CLIENT_MEM_FRACTION=`echo "scale=3;$MAXRAM / $GPUMEM"|bc`

N_RAM=$SLURM_MEM_PER_NODE
echo 'SLURM_MEM_PER_NODE:' $N_RAM
echo 'MAXRAM:' $MAXRAM
echo 'GPUMEM:' $GPUMEM
echo 'XLA_PYTHON_CLIENT_MEM_FRACTION:' $XLA_PYTHON_CLIENT_MEM_FRACTION

export N_CPU=$SLURM_NTASKS
echo running command: /usr/bin/time -v $PATH_TO_RF2NA/run_RF2NA.sh "$@" #!TODO: possible to optimize by running MSA in parallel
/usr/bin/time -v $PATH_TO_RF2NA/run_RF2NA.sh "$@"

GANGLIAENDTIME=`date "+%m%%2F%d%%2F%Y+%H%%3A%M"`
echo "Your Job $SLURM_JOB_NAME $SLURM_JOBID was running on node `hostname -s` using the GPU device(s) $CUDA_VISIBLE_DEVICES."; echo "GPU utilization record: $STARTURLGPU""&ce=""$GANGLIAENDTIME"

